Acidente
===============

Acidente is a font designed by Luís Camanho.

It was drafted in paper in 2009, to be used in a set of poster designs. The shapes are inspired by Akzidenz Grotesk.

In 2013 it was digitised, using Insckape and FontForge. The digital version was launched as part of the [OxShark Foundry](http://oxshark.org).

Contributors
=======

* Luís Camanho
* Ana Isabel Carvalho
* Ricardo Lafuente


Tools
=======

* GIMP
* Inkscape
* FontForge
* Tiny Type Tools


License
=======

Copyright (c) 2013, Luís Camanho. Acidente is licensed under the [SIL Open Font License](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).
